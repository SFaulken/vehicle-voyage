#!/bin/bash
ROOT_DIR=$(dirname $(realpath -s $0))
mkdir -p $ROOT_DIR/builds
docker run --rm -it -v $ROOT_DIR:/home/user/src/vv kdeorg/android-sdk bash -c "git clone --depth 1 kde:sysadmin/ci-tooling;/opt/helpers/build-kde-project kcoreaddons Frameworks -DBUILD_TESTING=OFF; /opt/helpers/build-kde-project kirigami Frameworks -DBUILD_TESTING=OFF;/opt/helpers/build-cmake vv vv -DQTANDROID_EXPORTED_TARGET=vehiclevoyage -DANDROID_APK_DIR=/home/user/src/vv/packaging/android -DANDROID_APK_OUTPUT_DIR=/home/user/src/vv/builds;make -C /home/user/build-arm/vv create-apk;make -C /home/user/build-arm64/vv create-apk"
