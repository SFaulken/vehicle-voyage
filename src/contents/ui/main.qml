import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts
import QtQuick.Controls 1.4 as Old
import QtQuick.Dialogs 1.3 as Dialogs
import Qt.labs.settings 1.0 as QtSettings

Kirigami.ApplicationWindow {
    id: root

    title: "Vehicle Voyage"
    
    ListModel {
        id: serviceTypeModel
        ListElement {value: 0; text: qsTr("Oil and Filter Change");}
        ListElement {value: 1; text: qsTr("Windshield Wiper Replacement");}
        ListElement {value: 2; text: qsTr("Coolant Change");}
        ListElement {value: 3; text: qsTr("Tire Rotation");}
        ListElement {value: 4; text: qsTr("Timing Belt Change");}
        ListElement {value: 5; text: qsTr("Serpentine Belt Changed");}
        ListElement {value: -1; text: qsTr("Other");}
    }
    
     contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }
    
    QtSettings.Settings {
        id: appAnalyticsSettings
        category: "analytics"
    }

    Kirigami.PageRouter {
        id: router
        initialRoute: "main"
        pageStack: root.pageStack.columnView
        
        MainPage{}
    
        Kirigami.PageRoute {
            name: "about"
            Component {
                Kirigami.AboutPage {
                    id: aboutPage
                    actions.main: Kirigami.Action {
                        iconName: "window-close"
                        text: qsTr("Close")
                        onTriggered: {
                            router.navigateToRoute("main");
                        }
                    }
                    aboutData: appAboutData
                }
            }
        }
        
        AddVehicle{}
        
        ServiceRecords{}
        
        AddRecord{}
        
        Settings{}
        
        EditRecord{}
    }
}
