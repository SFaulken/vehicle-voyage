import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts

Kirigami.PageRoute {
    name: "addvehicle"
    Component {
        Kirigami.Page {
            id: vehicleAddPage
            title: qsTr("Add Vehicle")
            Layouts.ColumnLayout {
                anchors.fill: parent
                Kirigami.InlineMessage {
                    Layouts.Layout.fillWidth: true
                    Layouts.Layout.leftMargin: 10
                    Layouts.Layout.rightMargin: 10
                    z: 9997
                    type: Kirigami.MessageType.Error
                    showCloseButton: true
                    id: formError
                }
                Kirigami.FormLayout {
                    Layouts.Layout.alignment: Qt.AlignHCenter
                    Layouts.Layout.fillWidth: true
                    width: vehicleAddPage.width
                    
                    Item {
                        Kirigami.FormData.isSection: true
                        Kirigami.FormData.label: "Vehicle Information"
                    }
                    
                    Controls.TextField {
                        id: nameField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Vehicle Name")+":"
                    }
                    
                    Controls.TextField {
                        id: makeField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Make")+":"
                    }
                    
                    Controls.TextField {
                        id: modelField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Model")+":"
                    }
                    
                    Controls.TextField {
                        id: yearField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Year")+":"
                        inputMethodHints: Qt.ImhDigitsOnly
                        validator: IntValidator {
                            bottom: 1900
                            top: 4000
                        }
                    }
                    
                    Controls.TextField {
                        id: vinField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("VIN")+":"
                    }
                    
                    Layouts.RowLayout {
                        
                        Controls.Button {
                            text: qsTr("Add")
                            highlighted: true
                            onClicked: {
                                if(yearField.acceptableInput) {
                                    vehicleModel.addNewVehicle(nameField.text, makeField.text, modelField.text, parseInt(yearField.text), "", vinField.text);
                                    router.popRoute();
                                } else {
                                    formError.text = qsTr("Year is invalid!");
                                    formError.visible = true;
                                }
                            }
                        }
                        
                        Controls.Button {
                            text: qsTr("Cancel")
                            onClicked: {
                                router.popRoute();
                            }
                        }
                    }
                }
            }
        }
    }
}
