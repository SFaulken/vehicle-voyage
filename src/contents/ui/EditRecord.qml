import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts
import QtQuick.Controls 1.4 as Old
import QtQuick.Dialogs 1.3 as Dialogs

Kirigami.PageRoute {
    name: "editrecord"
    Component {
        Kirigami.Page {
            id: editRecordPage
            title: qsTr("Edit Service Record")
            property var serviceId: Kirigami.PageRouter.data.id
            property var record: recordModel.getRecord(serviceId)
            Layouts.ColumnLayout {
                anchors.fill: parent
                Kirigami.InlineMessage {
                    Layouts.Layout.fillWidth: true
                    Layouts.Layout.leftMargin: 10
                    Layouts.Layout.rightMargin: 10
                    z: 9997
                    type: Kirigami.MessageType.Error
                    showCloseButton: true
                    id: formError
                }
                Kirigami.FormLayout {
                    Layouts.Layout.alignment: Qt.AlignHCenter
                    Layouts.Layout.fillWidth: true
                    width: editRecordPage.width
                    
                    Item {
                        Kirigami.FormData.isSection: true
                        Kirigami.FormData.label: "Service Information"
                    }
                    
                    Controls.TextField {
                        id: providerField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Service Provider")+":"
                        text: record["serviceprovider"]
                    }
                    
                    Controls.TextField {
                        id: typeField
                        enabled: false
                        text: record["servicetypename"]
                        Kirigami.FormData.label: qsTr("Service Type")+":"
                    }
                    
                    Layouts.RowLayout {
                        Kirigami.FormData.label: qsTr("Date")+":"
                        Controls.TextField {
                            id: dateField
                            text: new Date(record["servicedate"]).toLocaleDateString(Qt.locale())
                            readOnly: true
                        }
                        Controls.Button {
                            icon.name: "office-calendar"
                            text: qsTr("Select Date")
                            display: Controls.AbstractButton.IconOnly
                            onClicked: {
                                calendarPopup.open();
                            }
                        }
                    }
                    
                    Controls.TextField {
                        id: milesField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Miles")+":"
                        text: record["miles"]
                        inputMethodHints: Qt.ImhDigitsOnly
                        validator: IntValidator {
                            bottom: 0
                            top: 500000
                        }
                    }
                    
                    Controls.TextArea {
                        id: notesField
                        placeholderText: qsTr("Notes about service")
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Notes")+":"
                        text: record["notes"]
                    }
                    
                    Layouts.RowLayout {
                    
                        Controls.Button {
                            text: qsTr("Save")
                            highlighted: true
                            onClicked: {
                                var dateObj = Date.fromLocaleDateString(Qt.locale(), dateField.text);
                                if(milesField.acceptableInput) {
                                    recordModel.updateRecord(serviceId, providerField.text, dateObj.toISOString(), parseInt(milesField.text), notesField.text);
                                    router.popRoute();
                                } else {
                                    formError.text = qsTr("Invalid number of miles.");
                                    formError.visible = true;
                                }
                            }
                        }
                        Controls.Button {
                            text: qsTr("Cancel")
                            onClicked: {
                                router.popRoute();
                            }
                        }
                    }
                }
            }
            
            Kirigami.OverlaySheet {
                id: calendarPopup
                Old.Calendar {
                    selectedDate: Date.fromLocaleDateString(Qt.locale(), dateField.text)
                    onClicked: {
                        dateField.text = date.toLocaleDateString(Qt.locale());
                    }
                }
            }
        }
    }
}
