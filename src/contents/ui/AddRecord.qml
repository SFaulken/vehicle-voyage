import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts
import QtQuick.Controls 1.4 as Old
import QtQuick.Dialogs 1.3 as Dialogs

Kirigami.PageRoute {
    name: "addrecord"
    Component {
        Kirigami.Page {
            id: recordAddPage
            title: qsTr("Add Service Record")
            Layouts.ColumnLayout {
                anchors.fill: parent
                Kirigami.InlineMessage {
                    Layouts.Layout.fillWidth: true
                    Layouts.Layout.leftMargin: 10
                    Layouts.Layout.rightMargin: 10
                    z: 9997
                    type: Kirigami.MessageType.Error
                    showCloseButton: true
                    id: formError
                }
                Kirigami.FormLayout {
                    Layouts.Layout.alignment: Qt.AlignHCenter
                    Layouts.Layout.fillWidth: true
                    width: recordAddPage.width
                    
                    Item {
                        Kirigami.FormData.isSection: true
                        Kirigami.FormData.label: "Service Information"
                    }
                    
                    Controls.TextField {
                        id: providerField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Service Provider")+":"
                    }
                    
                    Controls.ComboBox {
                        id: typeField
                        model: serviceTypeModel
                        textRole: "text"
                        valueRole: "value"
                        onActivated: {
                            if(currentValue == -1) {
                                customTypeField.visible = true;
                            } else {
                                customTypeField.visible = false;
                            }
                        }
                        Kirigami.FormData.label: qsTr("Service Type")+":"
                    }
                    
                    Controls.TextField {
                        id: customTypeField
                        visible: false
                        Kirigami.FormData.label: qsTr("Custom Service Type")+":"
                    }
                    
                    Layouts.RowLayout {
                        Kirigami.FormData.label: qsTr("Date")+":"
                        Controls.TextField {
                            id: dateField
                            text: new Date().toLocaleDateString(Qt.locale())
                            readOnly: true
                        }
                        Controls.Button {
                            icon.name: "office-calendar"
                            text: qsTr("Select Date")
                            display: Controls.AbstractButton.IconOnly
                            onClicked: {
                                calendarPopup.open();
                            }
                        }
                    }
                    
                    Controls.TextField {
                        id: milesField
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Miles")+":"
                        inputMethodHints: Qt.ImhDigitsOnly
                        validator: IntValidator {
                            bottom: 0
                            top: 500000
                        }
                    }
                    
                    Controls.TextArea {
                        id: notesField
                        placeholderText: qsTr("Notes about service")
                        selectByMouse: true
                        Kirigami.FormData.label: qsTr("Notes")+":"
                    }
                    
                    Layouts.RowLayout {
                    
                        Controls.Button {
                            text: qsTr("Add")
                            highlighted: true
                            onClicked: {
                                var dateObj = Date.fromLocaleDateString(Qt.locale(), dateField.text);
                                var typeText = typeField.currentText;
                                if(typeField.currentValue == -1) {
                                    typeText = customTypeField.text;
                                }
                                if(milesField.acceptableInput) {
                                    recordModel.addNewRecord(providerField.text, typeField.currentValue, typeText, dateObj.toISOString(), parseInt(milesField.text), notesField.text);
                                    router.popRoute();
                                } else {
                                    formError.text = qsTr("Invalid number of miles.");
                                    formError.visible = true;
                                }
                            }
                        }
                        Controls.Button {
                            text: qsTr("Cancel")
                            onClicked: {
                                router.popRoute();
                            }
                        }
                    }
                }
            }
            
            Kirigami.OverlaySheet {
                id: calendarPopup
                Old.Calendar {
                    selectedDate: Date.fromLocaleDateString(Qt.locale(), dateField.text)
                    onClicked: {
                        dateField.text = date.toLocaleDateString(Qt.locale());
                    }
                }
            }
        }
    }
}
