import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts
import QtQuick.Controls 1.4 as Old

Kirigami.PageRoute {
    name: "main"
    Component {
        Kirigami.Page {
            id: mainPage
            mainAction: Kirigami.Action {
                text: qsTr("Add Vehicle")
                iconName: "list-add"
                onTriggered: {
                    router.navigateToRoute(["main","addvehicle"]);
                }
            }
            contextualActions: [
                Kirigami.Action {
                    iconName: "preferences-system"
                    text: qsTr("Settings")
                    onTriggered: {
                        router.navigateToRoute(["main","settings"]);
                    }
                },
                Kirigami.Action {
                    iconName: "help-about"
                    text: qsTr("About")
                    onTriggered: {
                        router.navigateToRoute("about");
                    }
                }
            ]
            title: "Vehicles"
            Kirigami.CardsListView {
                    anchors.fill: parent
                    id: vehicleView
                    model: vehicleModel
                    delegate: Kirigami.Card {
                        id: card
                        banner {
                            title: name
                        }
                        header: Row{
                            topPadding: 10.0
                            rightPadding: 10.0
                            leftPadding: 10.0
                            layoutDirection: Qt.RightToLeft
                            spacing: 5
                                Controls.Label {
                                    text: vin
                                    visible: root.wideScreen && mainPage.width > 550
                                    anchors.verticalCenter: parent.verticalCenter
                                }
                                Image {
                                    id: vinIcon
                                    source: "qrc:/license.svg"
                                    sourceSize.width: 32
                                    sourceSize.height: 32
                                    anchors.verticalCenter: parent.verticalCenter
                                    MouseArea {
                                        id: vinMouse
                                        hoverEnabled: true
                                        anchors.fill: vinIcon
                                    }
                                    Controls.ToolTip {
                                        id: vinTip
                                        text: vin
                                        visible: vinMouse.containsMouse
                                    }
                                }
                        }
                        contentItem: Item{
                            implicitHeight: Kirigami.Units.gridUnit * 4
                            Layouts.ColumnLayout {
                                Kirigami.Heading {
                                    text: maker + " " + vmodel
                                    level: 2
                                }
                                Kirigami.Heading {
                                    text: year
                                    level: 4
                                }
                                Kirigami.Icon {
                                    property bool oilChangeNeeded: false
                                    
                                    id: checkupIcon
                                    visible: oilChangeNeeded
                                    width: Kirigami.Units.iconSizes.smallMedium
                                    height: Kirigami.Units.iconSizes.smallMedium
                                    source: "dialog-information"
                                    isMask: true
                                    color: Kirigami.Theme.activeTextColor
                                    
                                    MouseArea {
                                        id: checkupMouse
                                        hoverEnabled: true
                                        anchors.fill: checkupIcon
                                    }
                                    Controls.ToolTip {
                                        id: checkupTip
                                        text: qsTr("Service is needed")
                                        visible: checkupMouse.containsMouse
                                    }
                                    
                                    Connections {
                                        target: dbAnalytics
                                        function onIsOilChangeNeeded(changeNeeded, vehicleId) {
                                            if(vehicleId == id) {
                                                checkupIcon.oilChangeNeeded = changeNeeded;
                                            }
                                        }
                                    }
                                    
                                    Component.onCompleted: {
                                        //Check vehicle warnings
                                        dbAnalytics.checkOilChangeNeededForVehicle(id, appAnalyticsSettings.value("milesForOilChange",5000), appAnalyticsSettings.value("monthsForOilChange",6));
                                    }
                                }
                            }
                        }
                        actions: [
                            Kirigami.Action {
                                text: qsTr("Examine Logs")
                                iconName: "edit-find"
                                onTriggered: {
                                    router.navigateToRoute(["main", {"route": "servicerecords", "data": {"id": id, "name": name}}]);
                                }
                            }
                        ]
                        hiddenActions: [
                            Kirigami.Action {
                                text: qsTr("Delete")
                                iconName: "edit-delete"
                                onTriggered: {
                                    vehicleModel.removeVehicle(index);
                                    router.navigateToRoute(["main"]);
                                }
                            }
                        ]
                    }
                    Controls.ScrollBar.vertical: Controls.ScrollBar {}
            }
        }
    }
}
