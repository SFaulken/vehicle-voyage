import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts

Kirigami.PageRoute {
    name: "servicerecords"
    
    Component {
        Kirigami.ScrollablePage {
            property var vehicleIndex: Kirigami.PageRouter.data.id
            Component.onCompleted: {
                recordModel.changeVehicleFilter(vehicleIndex);
                //Check vehicle warnings
                dbAnalytics.checkOilChangeNeededForVehicle(vehicleIndex, appAnalyticsSettings.value("milesForOilChange",5000), appAnalyticsSettings.value("monthsForOilChange",6));
            }
            Connections {
                target: recordModel
                function onVehicleUpdated(vehicleId) {
                    //Check vehicle warnings
                    if(vehicleId == vehicleIndex) {
                        dbAnalytics.checkOilChangeNeededForVehicle(vehicleId, appAnalyticsSettings.value("milesForOilChange",5000), appAnalyticsSettings.value("monthsForOilChange",6));
                    }
                }
            }
            id: serviceRecordPage
            title: qsTr("Service Records for ") + Kirigami.PageRouter.data.name
            mainAction: Kirigami.Action {
                text: qsTr("Add Service Record")
                iconName: "list-add"
                onTriggered: {
                    router.pushRoute({"route": "addrecord", "data": vehicleIndex});
                }
            }
            ListView {
                    id: serviceView
                    Layouts.Layout.fillHeight: true
                    Layouts.Layout.fillWidth: true
                    model: recordModel
                    headerPositioning: ListView.OverlayHeader
                    header: Kirigami.ListSectionHeader {
                        z: 9997
                        contentItem: Layouts.ColumnLayout {
                            Kirigami.InlineMessage {
                                Layouts.Layout.fillWidth: true
                                type: Kirigami.MessageType.Information
                                id: oilMessage
                                showCloseButton: false
                                text: qsTr("You are due for an oil change!")
                                Connections {
                                    target: dbAnalytics
                                    function onIsOilChangeNeeded(changeNeeded, vehicleId) {
                                        if(vehicleId == vehicleIndex) {
                                            oilMessage.visible = changeNeeded;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    delegate: Kirigami.SwipeListItem {
                            contentItem: Layouts.ColumnLayout {
                                Kirigami.Heading {
                                    text: serviceprovider
                                    level: 2
                                }
                                Kirigami.Heading {
                                    text: servicetypename
                                    level: 2
                                }
                                Controls.Label {
                                    text: new Date(servicedate).toLocaleDateString(Qt.locale())
                                }
                                Row {
                                    Image {
                                        source: "qrc:/speed.svg"
                                        sourceSize.width: 16
                                        sourceSize.height: 16
                                    }
                                    Controls.Label {
                                        text: miles
                                    }
                                }
                                Controls.Label {
                                    text: notes
                                }
                            }
                            actions: [
                                Kirigami.Action {
                                    text: qsTr("Remove")
                                    iconName: "edit-delete"
                                    onTriggered: {
                                        recordModel.removeRecord(index);
                                    }
                                },
                                Kirigami.Action {
                                    text: qsTr("Edit")
                                    iconName: "edit-symbolic"
                                    onTriggered: {
                                        if(router.routeActive(["main","servicerecords","editrecord"])) {
                                            router.popRoute();
                                        }
                                        router.pushRoute({"route": "editrecord", "data": {"id": index}});
                                    }
                                }
                            ]
                        }
            }
        }
    }
}
