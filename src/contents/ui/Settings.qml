import QtQuick 2.14
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14 as Layouts
import QtQuick.Dialogs 1.3 as Dialogs
import Qt.labs.platform 1.1 as Labs

Kirigami.PageRoute {
    name: "settings"
    Component {
        Kirigami.Page {
            id: settingsPage
            title: qsTr("Settings")
            contextualActions: [
                Kirigami.Action {
                    iconName: "application-javascript"
                    text: qsTr("Import From Carfax JSON")
                    onTriggered: {
                        importMessage.visible = true;
                    }
                }
            ]
            Dialogs.FileDialog {
                id: importDialog
                title: qsTr("Select Carfax Json file to import...")
                nameFilters: ["Carfax Vehicle Json (*.json)"]
                onAccepted: {
                    if(jsonConverter.importCarfaxJsonToDB(importDialog.fileUrl)) {
                        //
                    } else {
                        root.showPassiveNotification(qsTr("Failed to parse file."),"short");
                    }
                }
            }
            Labs.MessageDialog {
                id:importMessage
                title: qsTr("Warning")
                text: qsTr("This is an advanced feature that requires you pulling JSON file from Carfax website unofficially in advanced. Do you want to continue?")
                buttons: Labs.MessageDialog.Yes | Labs.MessageDialog.No
                onYesClicked: importDialog.visible = true
            }
            Layouts.ColumnLayout {
                anchors.fill: parent
                Kirigami.InlineMessage {
                    Layouts.Layout.fillWidth: true
                    Layouts.Layout.leftMargin: 10
                    Layouts.Layout.rightMargin: 10
                    z: 9997
                    type: Kirigami.MessageType.Error
                    showCloseButton: true
                    id: formError
                }
                Kirigami.FormLayout {
                    Layouts.Layout.alignment: Qt.AlignHCenter
                    Layouts.Layout.fillWidth: true
                    width: settingsPage.width
                    Item {
                        Kirigami.FormData.label: qsTr("Warning Thresholds")
                        Kirigami.FormData.isSection: true
                    }
                    Controls.TextField {
                        id: oilChangeMiles
                        Kirigami.FormData.label: qsTr("Oil Change Miles")
                        text: appAnalyticsSettings.value("milesForOilChange",5000)
                        validator: IntValidator {
                            bottom: 1
                            top: 500000
                        }
                    }
                    Controls.TextField {
                        id: oilChangeMonths
                        Kirigami.FormData.label: qsTr("Oil Change Months")
                        text: appAnalyticsSettings.value("monthsForOilChange",6)
                        validator: IntValidator {
                            bottom: 1
                            top: 500000
                        }
                    }
                    
                    Layouts.RowLayout {
                            
                            Controls.Button {
                                text: qsTr("Save")
                                highlighted: true
                                onClicked: {
                                    if(oilChangeMiles.acceptableInput) {
                                        if(oilChangeMonths.acceptableInput) {
                                            appAnalyticsSettings.setValue("milesForOilChange", parseInt(oilChangeMiles.text));
                                            appAnalyticsSettings.setValue("monthsForOilChange", parseInt(oilChangeMonths.text));
                                            router.popRoute();
                                        } else {
                                            formError.text = qsTr("Number of Months is invalid!");
                                            formError.visible = true;
                                        }
                                    } else {
                                        formError.text = qsTr("Number of Miles is invalid!");
                                        formError.visible = true;
                                    }
                                }
                            }
                            
                            Controls.Button {
                                text: qsTr("Cancel")
                                onClicked: {
                                    router.popRoute();
                                }
                            }
                        }
                }
            }
        }
    }
}
