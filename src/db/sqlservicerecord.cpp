/*
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sqlservicerecord.h"
#include <QSqlRecord>

SqlServiceRecord::SqlServiceRecord(QObject* parent, QSqlDatabase db) : QSqlTableModel(parent, db)
{
    setTable("records");
}


QHash<int, QByteArray> SqlServiceRecord::roleNames() const
{
    QHash<int, QByteArray> roles;
    // record() returns an empty QSqlRecord
    for (int i = 0; i < this->record().count(); i ++) {
        roles.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
    return roles;
}


QVariant SqlServiceRecord::data ( const QModelIndex& index, int role ) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if(role < Qt::UserRole)
    {
        value = QSqlQueryModel::data(index, role);
    }
    else
    {
        int columnIdx = role - Qt::UserRole - 1;
        QModelIndex modelIndex = this->index(index.row(), columnIdx);
        value = QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
    }
    return value;
}

void SqlServiceRecord::addNewRecord(QString serviceProvider, int serviceType, QString serviceTypeName, QString dateISO, int miles, QString notes)
{
    this->addNewRecord(this->currentVehicleID, serviceProvider, serviceType, serviceTypeName, dateISO, miles, notes);
}

void SqlServiceRecord::addNewRecord(int vehicleId, QString serviceProvider, int serviceType, QString serviceTypeName, QString dateISO, int miles, QString notes)
{
    QSqlRecord newItem = this->record();
    newItem.setValue("vehicle", vehicleId);
    newItem.setValue("serviceprovider", serviceProvider);
    newItem.setValue("servicetype", serviceType);
    newItem.setValue("servicetypename", serviceTypeName);
    newItem.setValue("servicedate", dateISO);
    newItem.setValue("miles", miles);
    newItem.setValue("notes", notes);
    this->insertRecord(-1, newItem);
    if(this->submitAll()) {
        printf("inserted new service record\n");
        this->database().commit();
        this->select();
        emit this->vehicleUpdated(this->currentVehicleID);
    } else {
        this->database().rollback();
        printf("database error\n");
    }
}

void SqlServiceRecord::removeRecord(int index)
{
    this->removeRow(index);
    if(this->submitAll()) {
        printf("removed service record\n");
        this->database().commit();
        this->select();
        emit this->vehicleUpdated(this->currentVehicleID);
    } else {
        this->database().rollback();
        printf("database error\n");
    }
}

void SqlServiceRecord::changeVehicleFilter(int id)
{
    this->setFilter("vehicle='"+QString::number(id)+"'");
    this->select();
    this->currentVehicleID = id;
}

QVariantMap SqlServiceRecord::getRecord(int index)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = this->index(index, 0);
    while(i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;

    }
    return res;
}

void SqlServiceRecord::updateRecord(int index, QString serviceProvider, QString dateISO, int miles, QString notes)
{
    QSqlRecord record = this->record(index);
    record.setValue("serviceprovider", serviceProvider);
    record.setValue("servicedate", dateISO);
    record.setValue("miles", miles);
    record.setValue("notes", notes);
    
    this->setRecord(index, record);
    this->database().commit();
    this->select();
    emit this->vehicleUpdated(this->currentVehicleID);
}







