/*
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECORDANALYTICS_H
#define RECORDANALYTICS_H

#include <qobject.h>
#include <QSqlDatabase>

/**
 * Class to analyze attributes of the data in the database. Used mostly for detecting service scheduling.
 */
class RecordAnalytics : public QObject
{
    Q_OBJECT
    
public:
    RecordAnalytics(QObject *parent = nullptr, QSqlDatabase db = QSqlDatabase());
    Q_INVOKABLE void checkOilChangeNeededForVehicle(int vehicleId, int milesForChange=5000, int monthsForChange=6);
    
signals:
    void isOilChangeNeeded(bool changeNeeded, int vehicleId);
    
private:
    QSqlDatabase _db;

};

#endif // RECORDANALYTICS_H
