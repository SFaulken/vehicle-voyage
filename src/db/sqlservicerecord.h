/*
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQLSERVICERECORD_H
#define SQLSERVICERECORD_H

#include <QSqlTableModel>
#include <QSqlRecord>

/**
 * Model to access service records.
 */
class SqlServiceRecord : public QSqlTableModel
{
    Q_OBJECT
public:
    SqlServiceRecord(QObject *parent = nullptr, QSqlDatabase db = QSqlDatabase());
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role) const override;
    Q_INVOKABLE void addNewRecord(int vehicleId, QString serviceProvider, int serviceType, QString serviceTypeName, QString dateISO, int miles, QString notes);
    Q_INVOKABLE void addNewRecord(QString serviceProvider, int serviceType, QString serviceTypeName, QString dateISO, int miles, QString notes);
    Q_INVOKABLE void removeRecord(int index);
    Q_INVOKABLE void changeVehicleFilter(int id);
    Q_INVOKABLE QVariantMap getRecord(int index);
    Q_INVOKABLE void updateRecord(int index, QString serviceProvider, QString dateISO, int miles, QString notes);
    
signals:
    void vehicleUpdated(int vehicleId);
private:
    int currentVehicleID = 0;

};

#endif // SQLSERVICERECORD_H
