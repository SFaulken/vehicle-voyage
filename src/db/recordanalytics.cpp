/*
 * Copyright (C) 2020  <Kevin Whitakerr> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "recordanalytics.h"
#include <QtConcurrent/QtConcurrent>
#include <QSqlQuery>
#include <QSqlRecord>

RecordAnalytics::RecordAnalytics(QObject* parent, QSqlDatabase db) : _db(db)
{
}

void RecordAnalytics::checkOilChangeNeededForVehicle(int vehicleId, int milesForChange, int monthsForChange)
{
    QtConcurrent::run([&](int vehicleId, int milesForChange, int monthsForChange){
        QSqlQuery query = _db.exec("SELECT * FROM records WHERE records.vehicle = "+QString::number(vehicleId));
        int milesRecord = query.record().indexOf("miles");
        int dateRecord = query.record().indexOf("servicedate");
        int typeRecord = query.record().indexOf("servicetype");
        QDateTime lastOilDate;
        int lastOilMiles = 0;
        int lastKnownMiles = 0;
        while(query.next()) {
            if(query.value(typeRecord).toInt() == 0) {
                if(!lastOilDate.isValid() || lastOilDate < QDateTime::fromString(query.value(dateRecord).toString(), Qt::DateFormat::ISODate)) {
                    lastOilDate = QDateTime::fromString(query.value(dateRecord).toString(), Qt::DateFormat::ISODate);
                    lastOilMiles = query.value(milesRecord).toInt();
                }
            }
            if(lastKnownMiles < query.value(milesRecord).toInt()) {
                lastKnownMiles = query.value(milesRecord).toInt();
            }
        }
        if(lastKnownMiles-lastOilMiles > milesForChange || lastOilMiles == 0 || !lastOilDate.isValid() || lastOilDate.daysTo(QDateTime::currentDateTime()) > 30*monthsForChange) {
            emit this->isOilChangeNeeded(true, vehicleId);
        } else {
            emit this->isOilChangeNeeded(false, vehicleId);
        }
    },vehicleId, milesForChange, monthsForChange);
}



