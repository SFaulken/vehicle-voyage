/*
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sqlvehicle.h"
#include <QSqlRecord>
#include <QSqlQuery>

SqlVehicle::SqlVehicle(QObject* parent, QSqlDatabase db) : QSqlTableModel(parent, db)
{
    setTable("vehicles");
}


QHash<int, QByteArray> SqlVehicle::roleNames() const
{
    QHash<int, QByteArray> roles;
    // record() returns an empty QSqlRecord
    for (int i = 0; i < this->record().count(); i ++) {
        roles.insert(Qt::UserRole + i + 1, record().fieldName(i).toUtf8());
    }
    return roles;
}


QVariant SqlVehicle::data ( const QModelIndex& index, int role ) const
{
    QVariant value = QSqlQueryModel::data(index, role);
    if(role < Qt::UserRole)
    {
        value = QSqlQueryModel::data(index, role);
    }
    else
    {
        int columnIdx = role - Qt::UserRole - 1;
        QModelIndex modelIndex = this->index(index.row(), columnIdx);
        value = QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
    }
    return value;
}

void SqlVehicle::addNewVehicle ( const QString name, const QString maker, const QString model, const int year, const QString image, const QString vin )
{
    QSqlRecord newItem = this->record();
    newItem.remove(newItem.indexOf("id"));
    newItem.setValue("name", name);
    newItem.setValue("maker", maker);
    newItem.setValue("vmodel", model);
    newItem.setValue("year", year);
    newItem.setValue("image", image);
    newItem.setValue("vin", vin);
    this->insertRecord(-1, newItem);
    if(this->submitAll()) {
        printf("inserted new vehicle record\n");
        this->database().commit();
        this->select();
    } else {
        this->database().rollback();
        printf("database error\n");
    }
}

void SqlVehicle::removeVehicle(int index)
{
    this->database().exec("DELETE FROM records WHERE records.vehicle="+this->record(index).value("id").toString());
    this->removeRow(index);
    if(this->submitAll()) {
        printf("removed vehicle record\n");
        this->database().commit();
        this->select();
    } else {
        this->database().rollback();
        printf("database error\n");
    }
}






