/*
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQLVEHICLE_H
#define SQLVEHICLE_H

#include <QSqlTableModel>

/**
 * Model to access list of vehicles
 */
class SqlVehicle : public QSqlTableModel
{
    Q_OBJECT
public:
    SqlVehicle(QObject *parent = nullptr, QSqlDatabase db = QSqlDatabase());
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role) const override;
    Q_INVOKABLE void addNewVehicle(const QString name, const QString maker, const QString model, const int year, const QString image, const QString vin);
    Q_INVOKABLE void removeVehicle(int index);
    
private:

};

#endif // SQLVEHICLE_H
