/*
 * Vehicle Voyage
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include <KAboutData>
#include <QIcon>
#include <QStandardPaths>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlError>
#include <QDebug>
#include <QSqlQuery>
#include <QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include "db/sqlvehicle.h"
#include "db/sqlservicerecord.h"
#include "jsonio.h"
#include "db/recordanalytics.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    QApplication app(argc, argv);
    
#ifdef Q_OS_ANDROID
    QQuickStyle::setStyle("Material");
#endif

    
    KAboutData aboutData("org.eyecreate.vehiclevoyage", "Vehicle Voyage", "1.1", "Track vehicle service history.",KAboutLicense::GPL_V3);//TODO:i18n
    aboutData.setProductName("vehiclevoyage");
    aboutData.addAuthor("Kevin Whitaker",QString(),"eyecreate@eyecreate.org","https://www.eyecreate.org");
    aboutData.setDesktopFileName("org.eyecreate.vehiclevoyage");
    
    QCoreApplication::setOrganizationName("eyecreate");
    QCoreApplication::setOrganizationDomain("eyecreate.org");
    QCoreApplication::setApplicationName(aboutData.productName());
    QCoreApplication::setApplicationVersion(aboutData.version());
    app.setWindowIcon(QIcon(":/logo.svg"));
    
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
#ifdef Q_OS_ANDROID
    if(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).length() > 1)
    {
        db.setDatabaseName(QDir(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[1]).filePath("servicerecords.sqlite")); //Use second appdata path on android for shared storage.
    }
    else
    {
        db.setDatabaseName(QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).filePath("servicerecords.sqlite"));
    }
#else
    db.setDatabaseName(QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).filePath("servicerecords.sqlite"));
#endif
    if(!QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).exists()) 
    {
        QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    }
    if(!QFile(db.databaseName()).exists()) 
    {
        QFile(db.databaseName()).open(QIODevice::ReadWrite);
        //Create table structure
        db.open();
        db.exec("CREATE TABLE vehicles (id INTEGER PRIMARY KEY, name TEXT, maker TEXT, vmodel TEXT, year INTEGER, image TEXT, vin TEXT);");
        db.exec("CREATE TABLE records (id INTEGER PRIMARY KEY, vehicle INTEGER, serviceprovider TEXT, servicetype INTEGER, servicetypename TEXT, servicedate TEXT, miles INTEGER, notes TEXT);");
        db.commit();
        db.close();
    }
    if(!db.open())
    {
        //Bad db, exit now.
        printf("%s",db.lastError().text().toLatin1().data());
        return -2;
    }
    
    SqlVehicle vehicles(nullptr, db);
    SqlServiceRecord records(nullptr, db);
    //Fill model with inital data.
    vehicles.select();
    records.select();
    JsonIO converter(&vehicles, &records);
    RecordAnalytics analytics(nullptr, db);
    
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("appAboutData"), QVariant::fromValue(aboutData));
    engine.rootContext()->setContextProperty(QStringLiteral("vehicleModel"), &vehicles);
    engine.rootContext()->setContextProperty(QStringLiteral("recordModel"), &records);
    engine.rootContext()->setContextProperty(QStringLiteral("jsonConverter"), &converter);
    engine.rootContext()->setContextProperty(QStringLiteral("dbAnalytics"), &analytics);
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));
    
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
