/*
 * Copyright (C) 2020  <Kevin Whitaker> <eyecreate@eyecreate.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JSONIO_H
#define JSONIO_H

#include <qobject.h>
#include "db/sqlvehicle.h"
#include "db/sqlservicerecord.h"

/**
 * Utility class to handle reading/writing json to/from DB.
 */
class JsonIO : public QObject
{
    Q_OBJECT
public:
    JsonIO(SqlVehicle *vehicles, SqlServiceRecord *records, QObject *parent = nullptr);
    Q_INVOKABLE bool importCarfaxJsonToDB(QString fileLocation);
    
private:
    SqlVehicle* vehicles;
    SqlServiceRecord* records;

};

#endif // JSONIO_H
